﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiDirectoryMonitoring.Models;

namespace WebApiDirectoryMonitoring.Configs
{
    public class ConfigProfile : Profile
    {
        public ConfigProfile()
        {
            CreateMap<DirectoryInform, DirectoryInfoViewModel>()
                .ForMember(dest => dest.DateOfChange, opt => opt.MapFrom(src => src.DateOfChange.ToString("dd.MM.yyyy HH:mm:ss")));
            CreateMap<DirectoryInfoViewModel, DirectoryInform>()
                 .ForMember(dest => dest.DateOfChange, opt => opt.MapFrom(src => Convert.ToDateTime(src.DateOfChange)));
        }
    }
}
