﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiDirectoryMonitoring.Interfaces;

namespace WebApiDirectoryMonitoring.Models
{
    public class DirectoryInfoRepository : IRepository<DirectoryInform>
    {
        private InfoContext db;
        readonly ILogger<DirectoryInfoRepository> _log;

        public DirectoryInfoRepository(InfoContext context, ILogger<DirectoryInfoRepository> log)
        {
            db = context;
            _log = log;
        }

        public async Task<IEnumerable<DirectoryInform>> GetAll()
        {
            return await db.DirectoryInforms.OrderBy(c=>c.DateOfChange).ToListAsync();
        }

        public async Task<IEnumerable<DirectoryInform>> GetByDate(string date)
        {
            return await db.DirectoryInforms.Where(c => c.DateOfChange.ToString("dd.MM.yyyy") == date).OrderBy(c=>c.DateOfChange).ToListAsync();
        }

        public async Task Create(DirectoryInform obj)
        {
            obj.Id = Guid.NewGuid();
            try
            {
                db.DirectoryInforms.Add(obj);
                await db.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _log.LogInformation(DateTime.Now.ToString() + e.Message + " DirectoryInfoRepository Create()");
            }
        }
    }
}
