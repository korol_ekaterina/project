﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiDirectoryMonitoring.Interfaces
{
   public interface IRepository<T> 
      where T : class
    {
        Task<IEnumerable<T>> GetAll();
        Task Create(T item);
        Task<IEnumerable<T>> GetByDate(string date);
    }
}
