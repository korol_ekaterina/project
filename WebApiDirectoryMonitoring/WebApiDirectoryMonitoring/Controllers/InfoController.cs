﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApiDirectoryMonitoring.Interfaces;
using WebApiDirectoryMonitoring.Models;

namespace WebApiDirectoryMonitoring.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InfoController : ControllerBase
    {
        private IRepository<DirectoryInform> _infoRepository;
        private readonly IMapper _mapper;

        public InfoController(IRepository<DirectoryInform> infoRepository, IMapper mapper)
        {
            _infoRepository = infoRepository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<DirectoryInfoViewModel>>> Get()
        {
            var collection = _mapper.Map<IEnumerable<DirectoryInform>, List<DirectoryInfoViewModel>>(await _infoRepository.GetAll());
            return collection;
        }

        [HttpGet("{date}")]
        public async Task<ActionResult<IEnumerable<DirectoryInfoViewModel>>> GetRecByDate(string date)
        {
            var collection = _mapper.Map<IEnumerable<DirectoryInform>, List<DirectoryInfoViewModel>>(await _infoRepository.GetByDate(date));
            return collection;
        }

        [HttpPost]
        public async void Post([FromBody] DirectoryInfoViewModel model)
        {
            DateTime date;
            bool success = DateTime.TryParse(model.DateOfChange, out date);
            if (success)
            {
                var obj = _mapper.Map<DirectoryInfoViewModel, DirectoryInform>(model);
                await _infoRepository.Create(obj);
            }
        }
    }
}