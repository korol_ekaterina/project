﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiDirectoryMonitoring.Models
{
    public class DirectoryInfoViewModel
    {
        public string Path { get; set; }
        public string TypeObj { get; set; }
        public string TypeEvent { get; set; }
        public string DateOfChange { get; set; }
    }
}
