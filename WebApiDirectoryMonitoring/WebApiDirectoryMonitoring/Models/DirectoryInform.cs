﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiDirectoryMonitoring.Models
{
    public class DirectoryInform
    {
        public Guid Id { get; set; }
        public string Path { get; set; }
        public string TypeObj { get; set; }
        public string TypeEvent { get; set; }
        public DateTime DateOfChange { get; set; }
    }
}
