﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiDirectoryMonitoring.Models
{
    public class InfoContext : DbContext
    {
        public DbSet<DirectoryInform> DirectoryInforms { get; set; }

        public InfoContext(DbContextOptions<InfoContext> options)
              : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
