﻿import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';
import { Model } from './model';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    providers: [DataService]
})

export class AppComponent implements OnInit {

    models: Model[];

    constructor(private dataService: DataService) { }

    ngOnInit() {
        this.loadObjects();
    }

    loadObjects() {
        this.dataService.getObjects()
            .subscribe((data: Model[]) => this.models = data);
    }
}