﻿export class Model {
    constructor(
        public path?: string,
        public typeObj?: string,
        public typeEvent?: string,
        public dateOfChange?: string) { }
}