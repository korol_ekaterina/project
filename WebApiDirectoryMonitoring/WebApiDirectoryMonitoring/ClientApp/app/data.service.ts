﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DataService {

    private url = "/api/info";

    constructor(private http: HttpClient) {
    }

    getObjects() {
        return this.http.get(this.url);
    }

}