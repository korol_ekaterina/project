var Model = /** @class */ (function () {
    function Model(path, typeObj, typeEvent, dateOfChange) {
        this.path = path;
        this.typeObj = typeObj;
        this.typeEvent = typeEvent;
        this.dateOfChange = dateOfChange;
    }
    return Model;
}());
export { Model };
//# sourceMappingURL=model.js.map