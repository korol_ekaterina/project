﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppFileMonitoring
{
    interface ICommand
    {
        void Execute();
        void Undo();
    }

    class HelpInformation
    {
        public void GetInfo()
        {
            Console.WriteLine("command -path displays path to directory");
            Console.WriteLine();
            Console.WriteLine("command -date displays changes in directory for the entered date\n"+
                "input command date in format -date dd.mm.yyyy ");
        }
    }

    class PathOfDirectory
    {
        public void GetPath()
        {
            Console.WriteLine(String.Format("Path to directory: {0}", Properties.Settings.Default.PathToCatalog));
        }
    }

    class InfoByDate
    {
        public void GetInfoByDate(string date)
        {
            Console.WriteLine("Info for " + date);
            var col = MethodsHttpClient.GetRecordsByDate(date);

            foreach (var el in col)
            {
                Console.WriteLine(String.Format("{0,30} {1,10} {2,10} {3,15}",
                    el.Path, el.TypeEvent, el.TypeObj, el.DateOfChange));
            }
        }
    }

    class Unkn
    {
        public void GetMessage()
        {
            Console.WriteLine("Unknown command..");
        }
    }

    class HelpCommand : ICommand
    {
        HelpInformation _help;

        public HelpCommand(HelpInformation help)
        {
            _help = help;
        }

        public void Execute()
        {
            _help.GetInfo();
        }

        public void Undo()
        {
        }
    }

    class PathCommand : ICommand
    {
        PathOfDirectory _path;

        public PathCommand(PathOfDirectory path)
        {
            _path = path;
        }

        public void Execute()
        {
            _path.GetPath();
        }

        public void Undo()
        {
        }
    }

    class InfoCommand : ICommand
    {
        InfoByDate _inf;
        string _date;

        public InfoCommand(InfoByDate inf, string date)
        {
            _date = date;
            _inf = inf;
        }

        public void Execute()
        {
            _inf.GetInfoByDate(_date);
        }

        public void Undo()
        {
        }
    }

    class UnkCommand : ICommand
    {
        Unkn _unkCom;

        public UnkCommand(Unkn unkCom)
        {
            _unkCom = unkCom;
        }

        public void Execute()
        {
            _unkCom.GetMessage();
        }

        public void Undo()
        {
        }
    }

    class InvokeCommand
    {
        ICommand command;

        public InvokeCommand() { }

        public void SetCommand(ICommand com)
        {
            command = com;
        }

        public void ExecCommand()
        {
            command.Execute();
        }
    }
}
