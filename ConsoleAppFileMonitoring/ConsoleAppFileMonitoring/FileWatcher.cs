﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppFileMonitoring
{
    public class FileWatcher
    {


        public void CreateWatcher()
        {
            string path = Properties.Settings.Default.PathToCatalog;
            if (File.Exists(path) || Directory.Exists(path))
            {
                FileSystemWatcher watcher = new FileSystemWatcher();
                watcher.Path = path;

                watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
               | NotifyFilters.FileName | NotifyFilters.DirectoryName;

                watcher.Changed += new FileSystemEventHandler(OnChanged);
                watcher.Created += new FileSystemEventHandler(OnChanged);
                watcher.Deleted += new FileSystemEventHandler(OnChanged);
                watcher.Renamed += new RenamedEventHandler(OnRenamed);

                watcher.EnableRaisingEvents = true;
                // watcher.IncludeSubdirectories = true;
            }
        }

        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            InfoModel model = new InfoModel
            {
                Path = e.FullPath,
                TypeObj = GetObjectType(e.FullPath),
                TypeEvent = e.ChangeType.ToString(),
                DateOfChange = DateTime.Now.ToString()
            };

            MethodsHttpClient.Create(model);
        }

        private static void OnRenamed(object source, RenamedEventArgs e)
        {
            InfoModel model = new InfoModel
            {
                Path = e.FullPath,
                TypeObj = GetObjectType(e.FullPath),
                TypeEvent = "Rename",
                DateOfChange = DateTime.Now.ToString()
            };

            MethodsHttpClient.Create(model);
        }

        private static string GetObjectType(string path)
        {
            if (File.Exists(path) || Directory.Exists(path))
            {
                FileAttributes attr = File.GetAttributes(path);
                return (attr & FileAttributes.Directory) == FileAttributes.Directory ? "catalog" : "file";
            }
            return String.Empty;
        }
    }
}
