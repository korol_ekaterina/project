﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppFileMonitoring
{
    public static class MethodsHttpClient
    {
        private const string APP_PATH = "http://localhost:55252/";

        public static async void Create(InfoModel model)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    HttpResponseMessage response = await client.PostAsJsonAsync(
                        APP_PATH + "api/info", model);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static List<InfoModel> GetRecordsByDate(string date)
        {
            List<InfoModel> col = new List<InfoModel>();
            try
            {
                using (var client = new HttpClient())
                {
                    var response = client.GetAsync(APP_PATH + "api/info/" + date).Result;
                    col = response.Content.ReadAsAsync<List<InfoModel>>().Result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return col;
        }
    }
}
