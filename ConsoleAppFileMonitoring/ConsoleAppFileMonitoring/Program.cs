﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppFileMonitoring
{
    class Program
    {

        static void Main(string[] args)
        {
            FileWatcher watcher = new FileWatcher();
            watcher.CreateWatcher();

            do {
                Console.WriteLine();
                Console.WriteLine("Input parameter:");
                var userparam = Console.ReadLine();

                InvokeCommand invC = new InvokeCommand();
                ICommand comm;
                string userdate = String.Empty;
                try
                {
                    if (userparam.Contains("-date")) { userdate = userparam.Split(' ')[1]; userparam = userparam.Split(' ')[0]; }
                }
                catch (Exception) { userparam = String.Empty; }

                switch (userparam)
                {
                    case "-help":
                        HelpInformation help = new HelpInformation();
                        comm = new HelpCommand(help);
                        break;
                    case "-path":
                        PathOfDirectory path = new PathOfDirectory();
                        comm = new PathCommand(path);
                        break;
                    case "-date":
                        InfoByDate inf = new InfoByDate();
                        comm = new InfoCommand(inf, userdate);
                        break;
                    default:
                        Unkn uncom = new Unkn();
                        comm = new UnkCommand(uncom);
                        break;
                }

                invC.SetCommand(comm);
                invC.ExecCommand();
            } while (true);
        }
    }
}
